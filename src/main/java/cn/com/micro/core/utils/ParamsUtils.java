package cn.com.micro.core.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
  * 请求参数
  * @ClassName: ParamsUtils
  * @author wanhonghui
  * @date 2018年9月28日 上午9:22:34
 */
public class ParamsUtils {
    /**
     * 解析出url参数中的键值对
     * 如 "index.jsp?Action=del&id=123"，解析出Action:del,id:123存入map中
     *
     * @param URL url地址
     * @return url请求参数部分
     */
    public static Map<String, String> URLRequest(String URL) {
        Map<String, String> mapRequest = new HashMap<String, String>();
        String[] arrSplit = null;
        String strUrlParam = TruncateUrlPage(URL);
        if (strUrlParam == null) {//说明没有前半部分
            //return mapRequest;
        	strUrlParam = URL;
        }
        //每个键值为一组 www.2cto.com
        arrSplit = strUrlParam.split("[&]");
        for (String strSplit : arrSplit) {
            String[] arrSplitEqual = null;
            arrSplitEqual = strSplit.split("[=]");
 
            //解析出键值
            if (arrSplitEqual.length > 1) {
                //正确解析
                mapRequest.put(arrSplitEqual[0], arrSplitEqual[1]);
 
            } else {
                if (arrSplitEqual[0] != "") {
                    //只有参数没有值，不加入
                    mapRequest.put(arrSplitEqual[0], "");
                }
            }
        }
        return mapRequest;
    }
    
    /**
     * 去掉url中的路径，留下请求参数部分
     *
     * @param strURL url地址
     * @return url请求参数部分
     */
    private static String TruncateUrlPage(String strURL) {
        String strAllParam = null;
        String[] arrSplit = null;
 
        strURL = strURL.trim();
 
        arrSplit = strURL.split("[?]");
        if (strURL.length() > 1) {
            if (arrSplit.length > 1) {
                if (arrSplit[1] != null) {
                    strAllParam = arrSplit[1];
                }
            }
        }
 
        return strAllParam;
    }
    
    
    
    /**
	 * 除去请求中的空值和签名参数
	 * @日期：2015年12月16日 下午5:22:35
	 * @param sArray
	 * @return 去掉空值与签名参数后的新签名参数
	 */
    public static Map<String, String> paraFilter(Map<String, String> sArray) {
        Map<String, String> result = new HashMap<String, String>();
        if (sArray == null || sArray.size() <= 0) {
            return result;
        }
        for (String key : sArray.keySet()) {
            String value = sArray.get(key);
            if (value == null || value.equals("") || key.equalsIgnoreCase("signature")|| key.equalsIgnoreCase("signatureType") || key.equalsIgnoreCase("from")  || key.equalsIgnoreCase("isappinstalled") ) {
                continue;
            }
            result.put(key, value);
        }
        return result;
    }
    
    /**
     * 把请求中所有参数排序，并按照“参数=参数值”的模式用“&”字符拼接成字符串
     * @日期：2015年12月16日 下午5:24:28
     * @param params 需要排序并参与字符拼接的请求参数
     * @return 拼接后字符串
     */
    public static String createLinkString(Map<String, String> params) {
        List<String> keys = new ArrayList<String>(params.keySet());
        Collections.sort(keys);
        String prestr = "";
        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            String value = params.get(key)==null ? "" : params.get(key);
            if (i == keys.size() - 1) {
            	//拼接时，不包括最后一个&字符
                prestr = prestr + key + "=" + value;
            } else {
                prestr = prestr + key + "=" + value + "&";
            }
        }
        return prestr;
    }
    
    /**
     * 
     * @日期：2015年12月16日 下午6:30:52
     * @param sPara 要签名的请求参数
     * @return 签名结果字符串
     */
	public static String buildRequestMysign(Map<String, String> sPara,String key,String input_charset) {
		sPara = paraFilter(sPara);
    	String prestr = createLinkString(sPara); 
    	//把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
        String mysign =  sign(prestr, key, input_charset);
        return mysign;
    }
	
	/**
	 * 构造签名的url
	 * @param url
	 * @param key
	 * @param input_charset
	 * @return
	 */
	public static String buildSignUrl(String url,String key,String input_charset){
		Map<String, String> params = URLRequest(url);
		String  signature  = buildRequestMysign(params, key, input_charset);
		if(url.contains("?")){
			url = url+"&signature="+signature;
		}else{
			url = url+"?signature="+signature;
		}
		return url;
	}
    
    
	
	/**
     * 签名字符串
     * @param text 需要签名的字符串
     * @param key 密钥
     * @param input_charset 编码格式
     * @return 签名结果
     */
    public static String sign(String text, String key, String input_charset) {
    	text = text +"&key="+ key;
    	try {  
            MessageDigest md = MessageDigest.getInstance("MD5");  
            md.update(getContentBytes(text, input_charset));  
            byte b[] = md.digest();  
  
            int i;  
  
            StringBuffer buf = new StringBuffer("");  
            for (int offset = 0; offset < b.length; offset++) {  
                i = b[offset];  
                if (i < 0)  
                    i += 256;  
                if (i < 16)  
                    buf.append("0");  
                buf.append(Integer.toHexString(i));  
            }  
            //32位加密  
            return buf.toString();  
            // 16位的加密  
            //return buf.toString().substring(8, 24);  
        } catch (NoSuchAlgorithmException e) {  
            return null;  
        }  
    	
    }
    
  
    /**
     * 获取字节数组
     * @param content 内容
     * @param charset 字符编码
     * @return 字节数组
     * @throws SignatureException
     * @throws UnsupportedEncodingException 
     */
    private static byte[] getContentBytes(String content, String charset) {
        if (charset == null || "".equals(charset)) {
            return content.getBytes();
        }
        try {
            return content.getBytes(charset);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("MD5签名过程中出现错误,指定的编码集不对,您目前指定的编码集是:" + charset);
        }
    }
    


}
