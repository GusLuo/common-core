
/**    
 * @文件名: ASCIISort.java 
 * @包名: cn.com.sinosoft.support.Util 
 * @描述: TODO(用一句话描述该文件做什么) 
 * @作者: LIUJIANFENG     
 * @时间: 2020年8月6日 上午11:57:59 
 * @version V1.0    
 */ 

package cn.com.micro.core.utils;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @类名: ASCIISortUtil
 * @描述: ASCII码排序
 * @作者: LIUJIANFENG
 * @时间: 2020年8月6日 上午11:58:52
 */
public class ASCIISortUtil {
	private static final Logger logger = LoggerFactory.getLogger(ASCIISortUtil.class);
	/**
	 * @创建时间: 2020年8月6日 上午11:59:07
	 * @更新时间: 2020年8月6日 上午11:59:07
	 * @作者: LIUJIANFENG
	 * @param str
	 * @return
	 * @描述: ASCII码排序
	 */
	public static String ASCIISort(String str) {
		char[] test = new char[str.length()];
		StringBuilder sb = new StringBuilder();
		while (true) {
			String a = str;// 直接读取这行当中的字符串。
			for (int i = 0; i < str.length(); i++) {
				// 字符串处理每次读取一位。
				test[i] = a.charAt(i);
			}
			Arrays.sort(test);
			for (int i = 0; i < test.length; i++) {
				sb.append(test[i]);
			}
			String trim = sb.toString().trim();
			logger.info("ASCIISort方法排序后的值位：" + trim);
			return trim;
		}
	}
}
