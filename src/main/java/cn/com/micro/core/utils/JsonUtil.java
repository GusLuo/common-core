package cn.com.micro.core.utils;


import java.io.IOException;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
/**
  * json工具类
  * @ClassName: JsonUtil
  * @author wanhonghui
  * @date 2017年12月7日 下午2:07:39
 */
public class JsonUtil {
    
	/**
	  * 对象转json
	  * @CreateTime: 2017年12月7日 下午2:07:59
	  * @UpdateTime: 2017年12月7日 下午2:07:59
	  * @param obj
	  * @return
	 */
    public static String toJson(Object obj) {
        return toJson(obj, "yyyy-MM-dd HH:mm:ss");
    }
    
    public static String toJson(Object obj,String dateFormat){
    	 try {
         	ObjectMapper objectMapper = new ObjectMapper();
         	objectMapper.setDateFormat(new SimpleDateFormat(dateFormat));
             return objectMapper.writeValueAsString(obj);
         } catch (JsonProcessingException e) {
             throw new RuntimeException(e);
         }
    } 
   
    /**
      * json转对象
      * @CreateTime: 2017年12月7日 下午2:08:15
      * @UpdateTime: 2017年12月7日 下午2:08:15
      * @param json
      * @param clazz
      * @return
     */
    public static <T> T toObject(String json, Class<T> clazz) {
        try {
        	ObjectMapper objectMapper = new ObjectMapper();
        	objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            return objectMapper.readValue(json, clazz);
        } catch (JsonParseException e) {
            throw new RuntimeException(e);
        } catch (JsonMappingException e) {
        	throw new RuntimeException(e);
        } catch (IOException e) {
        	throw new RuntimeException(e);
        }
    }

    public static <T> T toObject(String json,String dateFormat, TypeReference<T> valueTypeRef){
   	 	try {
        	ObjectMapper objectMapper = new ObjectMapper();
        	objectMapper.setDateFormat(new SimpleDateFormat(dateFormat));
        	return objectMapper.readValue(json, valueTypeRef);
        }catch (JsonParseException e) {
        	throw new RuntimeException(e);
        } catch (JsonMappingException e) {
        	throw new RuntimeException(e);
        } catch (IOException e) {
        	throw new RuntimeException(e);
        }
   } 
    /**
      * json转对象
      * @CreateTime: 2017年12月7日 下午2:08:31
      * @UpdateTime: 2017年12月7日 下午2:08:31
      * @param json
      * @param valueTypeRef
      * @return
     */
    public static <T> T toObject(String json, TypeReference<T> valueTypeRef) {
        try {
        	ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(json, valueTypeRef);
        } catch (JsonParseException e) {
            throw new RuntimeException(e);
        } catch (JsonMappingException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
