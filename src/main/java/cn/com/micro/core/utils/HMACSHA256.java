package cn.com.micro.core.utils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class HMACSHA256 {

	public static void main(String[] args) {
		String message = "00023338TC201902191731190001ZX007";
		String secret = "e1d9dab819c92c51fa4f9077589a60b4";
		System.out.println(sha256_HMAC(message, secret));
	}
	
	public static String sha256_HMAC(String message, String secret) {
        String hash = "";
        try {
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] bytes = sha256_HMAC.doFinal(message.getBytes());
            hash = byteArrayToHexString(bytes);
        } catch (Exception e) {
            System.out.println("Error HmacSHA256 ===========" + e.getMessage());
        }
        return hash;
	}
	
	 private static String byteArrayToHexString(byte[] b) {
	        StringBuilder hs = new StringBuilder();
	        String stmp;
	        for (int n = 0; b!=null && n < b.length; n++) {
	            stmp = Integer.toHexString(b[n] & 0XFF);
	            if (stmp.length() == 1)
	                hs.append('0');
	            hs.append(stmp);
	        }
	        return hs.toString().toLowerCase();
	    }

}
