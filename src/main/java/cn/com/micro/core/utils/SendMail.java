package cn.com.micro.core.utils;



import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * 发送邮件
 * @author Administrator
 *
 */
public class SendMail {

	private static Logger logger = LoggerFactory.getLogger(SendMail.class);

	private String host;
	
	private String formUser;
	
	private String password;
	
	
	public SendMail(String host, String formUser, String password) {
		this.host = host;
		this.formUser = formUser;
		this.password = password;
	}

	/**
	 * 
	 * @param content发送的内容
	 * @param title邮件标题
	 * @param toUsers 接收邮件的人，多个人用","分开
	 */
	public void sendMail(String title,String content,String toUsers){

		logger.info("进入到SendMail中的sendMail方法");
		try {
			logger.info("title="+title+";content="+content+";toUsers="+toUsers);
			Properties props=new Properties();//也可用Properties props = System.getProperties();
		
			props.put("mail.smtp.auth", "true");     
			props.put("mail.transport.protocol", "smtp");     
			props.put("mail.smtp.host", host);     
			props.put("mail.smtp.port", "25");    
		
			Session s=Session.getInstance(props,null);//根据属性新建一个邮件会话，null参数是一种Authenticator(验证程序) 对象
			s.setDebug(true);//设置调试标志,要查看经过邮件服务器邮件命令，可以用该方法
			logger.info("formUser="+formUser+";password="+password);
			Message msg=new MimeMessage(s);//由邮件会话新建一个消息对象a
			msg.setFrom(new InternetAddress(formUser)); //发件人  
			@SuppressWarnings("static-access")
			InternetAddress[] iaToList = new InternetAddress().parse(toUsers);  

			msg.setRecipients(Message.RecipientType.TO,iaToList); //收件人  
			msg.setSentDate(new Date()); // 发送日期     
			msg.setSubject(title); // 主题     
			msg.setText(content);
			// 邮件服务器进行验证     
			Transport tran = s.getTransport("smtp");   
			tran.connect(host, formUser, password);     
			tran.sendMessage(msg, msg.getAllRecipients()); // 
			logger.info("发送邮件成功");
		} catch (MessagingException e) {
			logger.error("发送邮件失败：", e);
		} 
	}
	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getFormUser() {
		return formUser;
	}

	public void setFormUser(String formUser) {
		this.formUser = formUser;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static void main(String[] args) {
		String host = "mail.aviva-cofco.com.cn";
		String formUser = "icaremail@aviva-cofco.com.cn";
		String password = "avivait@821";
		SendMail sm = new SendMail(host,formUser,password);
		sm.sendMail("111","232323","sinosoftgz1@aviva-cofco.com.cn");
	}
}
