package cn.com.micro.core.utils;

import java.util.UUID;

/**
  * UUID工具类
  * @ClassName: UuidUtils
  * @author yangqidu
  * @date 2018年9月3日 下午9:36:01
 */
public class UuidUtils {

	/**
	  * 获取32位的UUID，大写
	  * @CreateTime: 2018年9月3日 下午9:36:19
	  * @UpdateTime: 2018年9月3日 下午9:36:19
	  * @return
	 */
	public static String getUUID32(){
	    String uuid = UUID.randomUUID().toString().replace("-", "").toUpperCase();
	    return uuid;
	}
}
