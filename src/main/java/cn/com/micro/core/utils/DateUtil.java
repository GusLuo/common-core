package cn.com.micro.core.utils;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
  * 日期工具类
  * @ClassName: DateUtil
  * @author evan
  * @date 2017年12月25日 下午5:45:55
 */
public class DateUtil{
    public final static String YYYY = "yyyy";
    public final static String YYYY_MM_DD = "yyyy-MM-dd";
    public final static String YYYYMMDD = "yyyyMMdd";
    public final static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    /**
      * 获取年份
      * @CreateTime: 2017年12月25日 下午5:46:39
      * @UpdateTime: 2017年12月25日 下午5:46:39
      * @return
     */
    public static String getYear() {
    	return new SimpleDateFormat(YYYY).format(new Date());
    }

   
    /**
      * 比较日期 (日期比较，如果s>=e 返回true 否则返回false)
      * @CreateTime: 2017年12月25日 下午5:47:39
      * @UpdateTime: 2017年12月25日 下午5:47:39
      * @param s
      * @param e
      * @return 
     */
    public static boolean compareDate(String s, String e) {
        if (parseDate(s) == null || parseDate(e) == null) {
            return false;
        }
        return parseDate(s).getTime() >= parseDate(e).getTime();
    }
    
    /**
      * 格式化日期
      * @CreateTime: 2017年12月25日 下午5:53:31
      * @UpdateTime: 2017年12月25日 下午5:53:31
      * @param date
      * @return
     */
    public static String formatDate(Date date){
    	return formatDate(date, YYYY_MM_DD);
    }
    
    /**
      * 格式化日期
      * @CreateTime: 2017年12月25日 下午5:53:41
      * @UpdateTime: 2017年12月25日 下午5:53:41
      * @param date
      * @param format
      * @return
     */
    public static String formatDate(Date date,String format){
    	 DateFormat fmt = new SimpleDateFormat(format);
    	 return  fmt.format(date);
    }

    /**
      * 转换日期
      * @CreateTime: 2017年12月25日 下午5:48:32
      * @UpdateTime: 2017年12月25日 下午5:48:32
      * @param date
      * @return
     */
    public static Date parseDate(String date) {
    	return parseDate(date,YYYY_MM_DD);
    }
    /**
      * 转换日期
      * @CreateTime: 2017年12月25日 下午5:50:08
      * @UpdateTime: 2017年12月25日 下午5:50:08
      * @param date
      * @param format
      * @return
     */
    public static Date parseDate(String date,String format) {
        DateFormat fmt = new SimpleDateFormat(format);
        try {
            return fmt.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 校验日期是否合法
      * @CreateTime: 2017年12月25日 下午5:50:27
      * @UpdateTime: 2017年12月25日 下午5:50:27
      * @param s
      * @return
     */
    public static boolean isValidDate(String s) {
        return isValidDate(s,YYYY_MM_DD);
    }
    /**
      * 校验日期是否合法
      * @CreateTime: 2017年12月25日 下午5:51:15
      * @UpdateTime: 2017年12月25日 下午5:51:15
      * @param s
      * @param format 日期格式
      * @return
     */
    public static boolean isValidDate(String s,String format) {
        DateFormat fmt = new SimpleDateFormat(format);
        try {
            fmt.parse(s);
            return true;
        } catch (Exception e) {
            // 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
            return false;
        }
    }
    

    /**
     * 
     * @Description 获得时间年差
     * @param startTime
     * @param endTime
     * @return
     */
    public static int getDiffYear(String startTime, String endTime) {
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        try {
            int years = (int) (((fmt.parse(endTime).getTime() - fmt.parse(
                    startTime).getTime()) / (1000 * 60 * 60 * 24)) / 365);
            return years;
        } catch (Exception e) {
            // 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
            return 0;
        }
    }
    /**
     * 
     * @Description 获得时间分钟差
     * @param startTime
     * @param endTime
     * @return
     */
    public static int getDiffMinute(String startTime, String endTime) {
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            int minute = (int) ((fmt.parse(endTime).getTime() - fmt.parse(
                    startTime).getTime()) / 1000);
            return minute;
        } catch (Exception e) {
            // 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
            return 0;
        }
    }

    /**
      * 获得天数差
      * @CreateTime: 2017年12月25日 下午5:54:27
      * @UpdateTime: 2017年12月25日 下午5:54:27
      * @param beginDateStr
      * @param endDateStr
      * @return
     */
    public static long getDiffDay(String beginDateStr, String endDateStr) {
        long day = 0;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date beginDate = null;
        Date endDate = null;

        try {
            beginDate = format.parse(beginDateStr);
            endDate = format.parse(endDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
       return getDiffDay(beginDate, endDate);
    }
    
    public static long getDiffDay(Date beginDate, Date endDate) {
        long day = 0;
        day = (endDate.getTime() - beginDate.getTime()) / (24 * 60 * 60 * 1000);
        return day;
    }

    /**
     * 得到n天之后的日期
     * @param days
     * @return
     */
    public static String getAfterDayDate(String days) {
        return getAfterDayDate(days,YYYY_MM_DD);
    }
    
    public static String getAfterDayDate(String days,String format) {
        int daysInt = Integer.parseInt(days);

        Calendar canlendar = Calendar.getInstance(); // java.util包
        canlendar.add(Calendar.DATE, daysInt); // 日期减 如果不够减会将月变动
        Date date = canlendar.getTime();

        SimpleDateFormat sdfd = new SimpleDateFormat(format);
        String dateStr = sdfd.format(date);

        return dateStr;
    }
    
    public static String getAfterDayDate(Date oldDate,String days,String format) {
        int daysInt = Integer.parseInt(days);

        Calendar canlendar = Calendar.getInstance(); // java.util包
        canlendar.setTime(oldDate);
        canlendar.add(Calendar.DATE, daysInt); // 日期减 如果不够减会将月变动
        Date date = canlendar.getTime();

        SimpleDateFormat sdfd = new SimpleDateFormat(format);
        String dateStr = sdfd.format(date);

        return dateStr;
    }
    

    /**
     * 得到n天之后是周几
     * 
     * @param days
     * @return
     */
    public static String getAfterDayWeek(String days) {
        int daysInt = Integer.parseInt(days);

        Calendar canlendar = Calendar.getInstance(); // java.util包
        canlendar.add(Calendar.DATE, daysInt); // 日期减 如果不够减会将月变动
        Date date = canlendar.getTime();

        SimpleDateFormat sdf = new SimpleDateFormat("E");
        String dateStr = sdf.format(date);

        return dateStr;
    }

    public static int getAge(Date date){
		Calendar cal = Calendar.getInstance();
		if (cal.before(date)) {
			return -1;
		} else {
			int yearNow = cal.get(Calendar.YEAR);
			int monthNow = cal.get(Calendar.MONTH) + 1;
			int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
			cal.setTime(date);
			int yearBirth = cal.get(Calendar.YEAR);
			int monthBirth = cal.get(Calendar.MONTH)+1;
			int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);
			int age = yearNow - yearBirth;
			if (monthNow <= monthBirth) {
				if (monthNow == monthBirth) {
					// monthNow==monthBirth
					if (dayOfMonthNow < dayOfMonthBirth) {
						age--;
					}
				} else {
					// monthNow>monthBirth
					age--;
				}
			}
			return age;
		}
	}
}