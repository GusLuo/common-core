package cn.com.micro.core.utils;

import java.nio.charset.Charset;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommonUtil {

	private static Logger logger = LoggerFactory.getLogger(CommonUtil.class);

	/**
	 * 
	 * TODO 
	 * @创建时间: 2019年8月15日 上午10:04:57
	 * @更新时间: 2019年8月15日 上午10:04:57
	 * @作者: LUOXIAOHAI
	 * @param url 请求地址
	 * @param param json格式的参数
	 * @return
	 * @描述: 简要说明
	 */
	public static String sendHttpByRaw(String url, String param){

		try {
			
			logger.info("sendHttpByRaw方法获取的参数为:{}",param);  
			HttpClient httpClient =  HttpClientBuilder.create().build();  
			HttpPost post = new HttpPost(url);  
			StringEntity postingString  = new StringEntity(param.toString(), Charset.forName("UTF-8"));
			post.setEntity(postingString);
			post.addHeader("Content-type","application/json; charset=utf-8");

			post.setHeader("Accept", "application/json");

			HttpResponse response = httpClient.execute(post);  
			String content = EntityUtils.toString(response.getEntity());  
			logger.info("sendHttpByRaw方法返回结果:{}",content);  
			return content; 
		} catch (Exception e) {

			logger.error("sendHttpByRaw方法异常：", e);
		}
		return null;
	}
}
