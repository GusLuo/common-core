package cn.com.micro.core.utils;

import java.security.MessageDigest;

public class Md5Util {
	public static String md5(String temp) {
		  StringBuffer buf = new StringBuffer("");
		  try {
		   MessageDigest md = MessageDigest.getInstance("MD5");
		   md.update(temp.getBytes("UTF-8"));
		   byte[] b = md.digest();
		 
		   for (int j = 0; j < b.length; j++) {
		    int i = b[j];
		    if (i < 0)
		     i += 256;
		    if (i < 16)
		     buf.append("0");
		    buf.append(Integer.toHexString(i));
		   }
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  return buf.toString();
		 }
}
