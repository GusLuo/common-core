package cn.com.micro.core.utils;



import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 注意：这里的key(秘钥必须是16位的)
 * @author Administrator
 *
 */
public class AESUtil {

	private static Logger logger = LoggerFactory.getLogger(AESUtil.class);
	
	static final String algorithmStr = "AES/ECB/PKCS5Padding";


	private static byte[] encode(String content, String password) {
		
		logger.info("进入AESUtil中encode方法");
		try {
			byte[] keyStr = getKey(password);
			SecretKeySpec key = new SecretKeySpec(keyStr, "AES");
			Cipher cipher = Cipher.getInstance(algorithmStr);//algorithmStr          
			byte[] byteContent = content.getBytes("utf-8");
			cipher.init(Cipher.ENCRYPT_MODE, key);//   ʼ  
			byte[] result = cipher.doFinal(byteContent);
			logger.info("执行AESUtil中encode方法结束");
			return result; //     
		} catch (NoSuchAlgorithmException e) {
			logger.error("NoSuchAlgorithmException异常", e);
		} catch (NoSuchPaddingException e) {
			logger.error("NoSuchPaddingException异常", e);
		} catch (InvalidKeyException e) {
			logger.error("InvalidKeyException异常", e);
		} catch (UnsupportedEncodingException e) {
			logger.error("UnsupportedEncodingException异常", e);
		} catch (IllegalBlockSizeException e) {
			logger.error("IllegalBlockSizeException异常", e);
		} catch (BadPaddingException e) {
			logger.error("BadPaddingException异常", e);
		}catch (Exception e) {
			logger.error("Exception异常", e);
		}
		logger.info("执行AESUtil中encode方法结束");
		return null;
	}

	private static byte[] decode(byte[] content, String password) {
		logger.info("进入AESUtil中decode方法");
		try {
			byte[] keyStr = getKey(password);
			SecretKeySpec key = new SecretKeySpec(keyStr, "AES");
			Cipher cipher = Cipher.getInstance(algorithmStr);//algorithmStr           
			cipher.init(Cipher.DECRYPT_MODE, key);//   ʼ  
			byte[] result = cipher.doFinal(content);
			logger.info("执行AESUtil中decode方法结束");
			return result; //     
		} catch (NoSuchAlgorithmException e) {
			logger.error("NoSuchAlgorithmException异常", e);
		} catch (NoSuchPaddingException e) {
			logger.error("NoSuchPaddingException异常", e);
		} catch (InvalidKeyException e) {
			logger.error("InvalidKeyException异常", e);
		} catch (IllegalBlockSizeException e) {
			logger.error("IllegalBlockSizeException异常", e);
		} catch (BadPaddingException e) {
			logger.error("BadPaddingException异常", e);
		}
		return null;
	}

	private static byte[] getKey(String password) {
		byte[] rByte = null;
		if (password!=null) {
			rByte = password.getBytes();
		}else{
			rByte = new byte[24];
		}
		return rByte;
	}

	/**
	 * 将二进制转换成16进制
	 * @param buf
	 * @return
	 */
	public static String parseByte2HexStr(byte buf[]) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < buf.length; i++) {
			String hex = Integer.toHexString(buf[i] & 0xFF);
			if (hex.length() == 1) {
				hex = '0' + hex;
			}
			sb.append(hex.toUpperCase());
		}
		return sb.toString();
	}

	/**
	 * 将16进制转换为二进制
	 * @param hexStr
	 * @return
	 */
	public static byte[] parseHexStr2Byte(String hexStr) {
		if (hexStr.length() < 1)
			return null; 
		byte[] result = new byte[hexStr.length() / 2];
		for (int i = 0; i < hexStr.length() / 2; i++) {
			int high = Integer.parseInt(hexStr.substring(i * 2, i * 2 + 1), 16);
			int low = Integer.parseInt(hexStr.substring(i * 2 + 1, i * 2 + 2),
					16);
			result[i] = (byte) (high * 16 + low);
		}
		return result;
	}
	
	/**
	 *加密
	 */
	public static String encrypt(String content,String keyBytes){
		//加密之后的字节数组,转成16进制的字符串形式输出
		logger.info("content={},keyBytes={}",new Object[]{content,keyBytes});
		return parseByte2HexStr(encode(content, keyBytes));
	}

	/**
	 *解密
	 */
	public static String decrypt(String content,String keyBytes){
		//解密之前,先将输入的字符串按照16进制转成二进制的字节数组,作为待解密的内容输入
		logger.info("content={},keyBytes={}",new Object[]{content,keyBytes});
		byte[] b = decode(parseHexStr2Byte(content), keyBytes);
		return new String(b);
	}


}
