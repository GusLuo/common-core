package cn.com.micro.core.utils;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONException;

public class HttpClientService {
	private String serviceUrl;
	private String charset = "utf-8";
	private Map<String, Object> hashMap = new HashMap<String, Object>(); 
	
	private static Logger logger = LoggerFactory.getLogger(HttpClientService.class);
	
	/**
	 * 构造方法
	 * @param serviceUrl	服务器地址
	 */
	public HttpClientService(String serviceUrl){
		this.serviceUrl = serviceUrl;
	}
	
	/**
	 * 构造方法
	 * @param serviceUrl	服务器地址
	 * @param charset 编码方式
	 */
	public HttpClientService(String serviceUrl, String charset){
		this.serviceUrl = serviceUrl;
		this.charset = charset;
	}
	
	
	/**
	 * 添加参数
	 * @param key	
	 * @param object
	 */
	public void addParameter(String key, String value) {
		hashMap.put(key, value);
	}
	
	public String sendPostRequest(){
		StringBuffer sb = new StringBuffer();
		try{
			logger.info("调用地址:" + serviceUrl);
			URL mUrl = new URL(serviceUrl);
			HttpURLConnection conn = (HttpURLConnection) mUrl.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(false);
			// 设置POST
			conn.setRequestProperty("Content-type",
					"application/x-www-form-urlencoded");
			conn.connect();
			DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
			// 封装HTTP参数
			Iterator<Entry<String, Object>> iter = hashMap.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry<String, Object> entry = (Map.Entry<String, Object>) iter.next();
				String key = (String) entry.getKey();
				String value = (String) entry.getValue();
				sb.append(key).append("=").append(value).append("&");
			}
			if(sb.length() > 1){
				dos.write(sb.substring(0, sb.length() - 1).getBytes(charset));
			}
			
			dos.flush();
			dos.close();
			// 获取HTTP请求状态码
			int responseCode = conn.getResponseCode();
			// HTTP请求成功
			if (responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), charset));
				String line;
				// 清空字符缓存
				sb.delete(0, sb.length());
				// 循环获取应答信息
				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}
				reader.close();
				conn.disconnect();
			} else{// HTTP请求失败
//				throw new IOException("Service is Bad!");
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	
	
	/**
	 * 发送https请求
	 * @param url 请求的地址
	 * count:表示请求的次数
	 * @return
	 * 如果报的异常为java.net.SocketException: Unexpected end of file from server
	 * 则重新发送请求，同一个人发送请求不得超过5次
	 */
	public static String sendHTTPS(String url){
	   logger.info("进入WeChatUtil中的sendHTTPS方法中");
	   StringBuffer lines = new StringBuffer(); 
	   BufferedReader in = null;
	   try {
	       logger.info("请求地址url={}"+url);
	           trustAllHttpsCertificates();
	           URLConnection connection = new URL(url).openConnection();
	           connection.setDoInput(true);
	           connection.setUseCaches(false);
	           in = new BufferedReader(new InputStreamReader(connection.getInputStream()));    
	           String line;
	           while ((line = in.readLine()) != null) {
	              lines.append(line);
	           }
	       } catch (IOException e) {
	           logger.error("发送https请求异常IOException,重新请求一次!IOException={}" + e.toString());
	       } catch (JSONException e) {
	           logger.error("发送https请求异常JSONException={}",e);
	       } catch (Exception e) {
	 
	           logger.error("发送https请求异常Exception={}",e);
	   } finally {
	       if (in != null) {
	          try {in.close();} catch (IOException e) {e.printStackTrace();}
	       }
	   }
	   logger.info("执行WeChatUtil中的sendHTTPS方法结束");
	   return lines.toString();
	}
	    
	/*************************https请求证书信任管理器 start ******************************************************************************************/
	private static void trustAllHttpsCertificates() throws Exception {
	   javax.net.ssl.TrustManager[] trustAllCerts = new javax.net.ssl.TrustManager[1];
	   javax.net.ssl.TrustManager tm = new miTM();
	   trustAllCerts[0] = tm;
	   javax.net.ssl.SSLContext sc = javax.net.ssl.SSLContext
	          .getInstance("TLS");
	   sc.init(null, trustAllCerts, null);
	   javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(sc
	          .getSocketFactory());
	}
	static class miTM implements javax.net.ssl.TrustManager,
	javax.net.ssl.X509TrustManager {
	   public java.security.cert.X509Certificate[] getAcceptedIssuers() {
	       return null;
	   }
	
	   public boolean isServerTrusted(
	          java.security.cert.X509Certificate[] certs) {
	       return true;
	   }
	
	   public boolean isClientTrusted(
	          java.security.cert.X509Certificate[] certs) {
	       return true;
	   }
	
	   public void checkServerTrusted(
	          java.security.cert.X509Certificate[] certs, String authType)
	                 throws java.security.cert.CertificateException {
	       return;
	   }
	
	   public void checkClientTrusted(
	          java.security.cert.X509Certificate[] certs, String authType)
	                 throws java.security.cert.CertificateException {
	       return;
	   }
	}
	/***************************https请求证书信任管理器 end****************************************************************************************/
	
	
	/**
	 * 发送https请求
	 * @param url 请求的地址 POST
	 * @return
	 */
	public static String sendHTTPS(String url, String param){
	   logger.info("进入HttpClientService中的sendHTTPS方法，参数：url="+url+",param="+param);
	   PrintWriter out = null;
	   StringBuffer lines = new StringBuffer(); 
	   BufferedReader in = null;
	   try {
           trustAllHttpsCertificates();
           URLConnection connection = new URL(url).openConnection();
        // 设置通用的请求属性
           connection.setRequestProperty("accept", "*/*");
           connection.setRequestProperty("connection", "Keep-Alive");
           connection.setRequestProperty("Content-type","application/json;charset=utf-8");
           connection.setRequestProperty("user-agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
           connection.setRequestProperty("Charset", "utf-8");
           connection.setDoInput(true);
           connection.setDoOutput(true);
           connection.setUseCaches(false);
           connection.setConnectTimeout(30000);
           // 设置从主机读取数据超时
           connection.setReadTimeout(30000);
           // 获取URLConnection对象对应的输出流
           out = new PrintWriter(connection.getOutputStream());
           // 发送请求参数
           out.print(param);
			// flush输出流的缓冲
           out.flush();
           in = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
           String line;
           while ((line = in.readLine()) != null) {
              lines.append(line);
           }
       } catch (IOException e) {
           logger.error("发送https请求异常IOException={}",e);
       } catch (JSONException e) {
           logger.error("发送https请求异常JSONException={}",e);
       } catch (Exception e) {
           logger.error("发送https请求异常Exception={}",e);
       } finally {
	       if (in != null) {
	          try {
	        	  in.close();
	          } catch (IOException e) {
	        	  e.printStackTrace();
	          }
	       }
       }
	   logger.info("HttpClientService中的sendHTTPS方法结束");
	   return lines.toString();
	}
	
	 public static String sendPost(String url, String param) {
	        PrintWriter out = null;
	        BufferedReader in = null;
	        String result = "";
	        try {
	            URL realUrl = new URL(url);
	            // 打开和URL之间的连接
	            URLConnection conn = realUrl.openConnection();
	            // 设置通用的请求属性
	            conn.setRequestProperty("accept", "*/*");
	            conn.setRequestProperty("connection", "Keep-Alive");
	            conn.setRequestProperty("user-agent",
	                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
	            // 发送POST请求必须设置如下两行
	            conn.setDoOutput(true);
	            conn.setDoInput(true);
	            // 获取URLConnection对象对应的输出流
	            out = new PrintWriter(conn.getOutputStream());
	            // 发送请求参数
	            out.print(param);
//	            out.print(param.getBytes("utf-8"));
	            // flush输出流的缓冲
	            out.flush();
	            // 定义BufferedReader输入流来读取URL的响应
	            in = new BufferedReader(
	                    new InputStreamReader(conn.getInputStream(), "utf-8"));
//	            in = new BufferedReader(
//	            		new InputStreamReader(conn.getInputStream(), "utf-8"));
	            String line;
	            while ((line = in.readLine()) != null) {
	                result += line;
	            }
	        } catch (Exception e) {
	            System.out.println("发送 POST 请求出现异常！"+e);
	            e.printStackTrace();
	        }
	        //使用finally块来关闭输出流、输入流
	        finally{
	            try{
	                if(out!=null){
	                    out.close();
	                }
	                if(in!=null){
	                    in.close();
	                }
	            }
	            catch(IOException ex){
	                ex.printStackTrace();
	            }
	        }
	        return result;
	    }
	    
	
}