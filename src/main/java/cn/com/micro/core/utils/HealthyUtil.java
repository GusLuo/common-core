package cn.com.micro.core.utils;

import java.io.StringReader;
import java.net.URL;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;

public class HealthyUtil {

	private static Logger logger = LoggerFactory.getLogger(HealthyUtil.class);
	
	/**
	 * @创建时间: 2020年12月1日 下午3:08:48
	 * @更新时间: 2020年12月1日 下午3:08:48
	 * @作者: LIUJIANFENG
	 * @param request
	 * @param mobile
	 * @param sendData
	 * @return
	 * @描述:  发送短信 
	 */
	public static boolean sendPhoneNoCode(HttpServletRequest request, String mobile, String sendData,String url,String qname) {
		// 短信接口地址和qname
		logger.info("短信接口RL={},qname={}", url, qname);
		String result = "";
		// 拼接发送的报文
		StringBuilder xml = new StringBuilder();
		xml.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
		xml.append("<Messages>");
		xml.append("\t<PublicInfo>");
		xml.append("\t\t<SystemCode>WEB</SystemCode>");
		// 这里由于如果不带code就会报错，所以填写一个code 但是不按照该code对应模板发送
		xml.append("\t\t<ServiceCode>ECSS012</ServiceCode>");
		xml.append("\t\t<User>" + md5("WEBadmin").toUpperCase() + "</User>");
		xml.append("\t\t<Password>" + md5("WEBadmin123").toUpperCase() + "</Password>");
		xml.append("\t</PublicInfo>");
		xml.append("\t<Message>");
		xml.append(
				"\t\t<MessageId>WebCS" + new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()) + "</MessageId>");
		xml.append("\t\t<MobileNums>");
		xml.append("\t\t\t<MobileNum>").append(mobile).append("</MobileNum>");
		xml.append("\t\t</MobileNums>");
		xml.append("\t\t<SendData>").append(sendData).append("</SendData>");
		// 此处datatype为1的话 代表不用模板
		xml.append("\t\t<DataType>1</DataType>");
		xml.append("\t\t<SendWay>0</SendWay>");
		xml.append("\t\t<CustomerID></CustomerID>");
		xml.append("\t\t<ServiceType>2</ServiceType>");
		xml.append("\t\t<InsuranceType >CS</InsuranceType>");
		xml.append("\t\t<UnitCode>0000000</UnitCode>");
		xml.append("\t\t<DealOrder>0</DealOrder>");
		xml.append("\t\t<AnswerMatch>0</AnswerMatch>");
		xml.append("\t</Message>");
		xml.append("</Messages>");

		logger.info("短信接口 请求报文  xml={}",xml.toString());
		// 开始调用接口
		Service service = new Service();
	
		Call call;
		try {
			call = (Call)service.createCall();
			call.setTargetEndpointAddress(new URL(url));
			call.setOperationName(new QName(qname, "send"));
			// 调用接口
			result = (String) call.invoke(new Object[] { xml.toString() });
			// 接口返回信息
			logger.info("短信接口 返回报文  xml={}",result);
			// 解析接口返回信息
			Document document = DocumentHelper.parseText(result);
			@SuppressWarnings("unchecked")
			List<Node> nodes = document.selectNodes("/TXLife/TransResult");
			String transResult = nodes.get(0).getText();
			logger.info("短信接口发送短信结果transResult={}",transResult);
			// 如果调用成功，返回true；
			if ("1".equals(transResult)) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	
	public static boolean sendEnjoyPrize(String... strs) {
		boolean flag = false;
		try {
			StringBuilder xml = new StringBuilder();
	        xml.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
	        xml.append("<Messages>");
	        xml.append("\t<PublicInfo>");
	        xml.append("\t\t<SystemCode>WEB</SystemCode>");
	        //短信编码
	        xml.append("\t\t<ServiceCode>"+strs[1]+"</ServiceCode>");
	        xml.append("\t\t<User>" + md5("WEBadmin").toUpperCase() + "</User>");
	        xml.append("\t\t<Password>" + md5("WEBadmin123").toUpperCase() + "</Password>");
	        xml.append("\t</PublicInfo>");
	        xml.append("\t<Message>");
	        xml.append("\t\t<MessageId>WebCS" + new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new java.util.Date()) + "</MessageId>");
	        xml.append("\t\t<MobileNums>");
	        //手机号
	        xml.append("\t\t\t<MobileNum>").append(strs[2]).append("</MobileNum>");
	        xml.append("\t\t</MobileNums>");
	        
	        if("WBSS017".equals(strs[1])){//癌症筛查
	        	//依次传入<Parm1>：客户名，<Parm2>：卡号，<Parm3>：密码，<Parm4>：有效期
	        	xml.append("\t\t<SendData>").append(strs[4]).append(";").append(strs[5]).append(";").append(strs[6]).append(";").append(strs[7]).append("</SendData>");
	        }else if("WBSS018".equals(strs[1]) || "WBSS019".equals(strs[1]) || "WBSS020".equals(strs[1])){//贵宾出行服务
	        	//依次传入<Parm1>：客户名，<Parm2>：有效期
	        	xml.append("\t\t<SendData>").append(strs[4]).append(";").append(strs[5]).append("</SendData>");
	        }
	        
	        xml.append("\t\t<DataType>0</DataType>");
	        xml.append("\t\t<SendWay>0</SendWay>");
	        xml.append("\t\t<CustomerID>").append(strs[3]).append("</CustomerID>");
	        xml.append("\t\t<ServiceType>2</ServiceType>");
	        xml.append("\t\t<InsuranceType >CS</InsuranceType>");
	        xml.append("\t\t<UnitCode>0000000</UnitCode>");
	        xml.append("\t\t<DealOrder>0</DealOrder>");
	        xml.append("\t\t<AnswerMatch>0</AnswerMatch>");
	        xml.append("\t</Message>");
	        xml.append("</Messages>"); 
			logger.info("发送短信xml:{}",xml);

			String operation = "send";
			org.apache.axis.client.Service service = new org.apache.axis.client.Service();
			String result = "";
			Call call = (Call) service.createCall();
			call.setTargetEndpointAddress(new URL(strs[0]));
			call.setOperationName(new QName("http://webservice.sms.sinosoft.com", operation));
			result = (String) call.invoke(new Object[] { xml.toString() });

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			org.w3c.dom.Document doc = builder.parse(new InputSource(new StringReader(result)));
			String transResult = doc.getElementsByTagName("TransResult").item(0).getFirstChild().getNodeValue();
			String errMsg = doc.getElementsByTagName("ErrMsg").item(0).getFirstChild().getNodeValue();
			logger.info("发送短信结果：{}",errMsg);
			flag = transResult.equals("1");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("发送短信出错，错误信息：{}",e);
		}
		return flag;
	}
	
	
	//安心陪诊发送短信模板
		public static boolean sendCarePrizeChange(String url,String mobile, String name,String epolicyCardNo,String epolicyVerifycode,String epolicyExpDate, String custno) {
			boolean flag = false;
			try {
				StringBuilder xml = new StringBuilder();
		        xml.append("<?xml version=\"1.0\" encoding=\"gb2312\"?>");
		        xml.append("<Messages>");
		        xml.append("\t<PublicInfo>");
		        xml.append("\t\t<SystemCode>WEB</SystemCode>");
		        xml.append("\t\t<ServiceCode>WBSS004</ServiceCode>");
		        xml.append("\t\t<User>" + md5("WEBadmin").toUpperCase() + "</User>");
		        xml.append("\t\t<Password>" + md5("WEBadmin123").toUpperCase() + "</Password>");
		        xml.append("\t</PublicInfo>");
		        xml.append("\t<Message>");
		        xml.append("\t\t<MessageId>WebCS" + new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new java.util.Date()) + "</MessageId>");
		        xml.append("\t\t<MobileNums>");
		        xml.append("\t\t\t<MobileNum>").append(mobile).append("</MobileNum>");
		        xml.append("\t\t</MobileNums>");
		        xml.append("\t\t<SendData>").append(name).append(";").append(epolicyCardNo).append(";").append(epolicyVerifycode).append(";").append(epolicyExpDate).append("</SendData>");
		        xml.append("\t\t<DataType>0</DataType>");
		        xml.append("\t\t<SendWay>0</SendWay>");
		        xml.append("\t\t<CustomerID>").append(custno).append("</CustomerID>");
		        xml.append("\t\t<ServiceType>2</ServiceType>");
		        xml.append("\t\t<InsuranceType >CS</InsuranceType>");
		        xml.append("\t\t<UnitCode>0000000</UnitCode>");
		        xml.append("\t\t<DealOrder>0</DealOrder>");
		        xml.append("\t\t<AnswerMatch>0</AnswerMatch>");
		        xml.append("\t</Message>");
		        xml.append("</Messages>"); 
				logger.info("发送短信xml:{}",xml);

				String operation = "send";
				org.apache.axis.client.Service service = new org.apache.axis.client.Service();
				String result = "";
				Call call = (Call) service.createCall();
				call.setTargetEndpointAddress(new URL(url));
				call.setOperationName(new QName(
						"http://webservice.sms.sinosoft.com", operation));
				result = (String) call.invoke(new Object[] { xml.toString() });

				DocumentBuilderFactory factory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				org.w3c.dom.Document doc = builder.parse(new InputSource(new StringReader(result)));
				String transResult = doc.getElementsByTagName("TransResult").item(0).getFirstChild().getNodeValue();
				String errMsg = doc.getElementsByTagName("ErrMsg").item(0).getFirstChild().getNodeValue();
				logger.info("发送短信结果：{}",errMsg);
				flag = transResult.equals("1");
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("发送短信出错，错误信息：{}",e);
			}
			return flag;
		}
	
	
	/**
	 * @创建时间: 2020年12月1日 下午3:07:44
	 * @更新时间: 2020年12月1日 下午3:07:44
	 * @作者: LIUJIANFENG
	 * @param temp
	 * @return
	 * @描述: MD5加密
	 */
	public static String md5(String temp) {
		StringBuffer buf = new StringBuffer("");
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(temp.getBytes());
			byte[] b = md.digest();

			for (int j = 0; j < b.length; j++) {
				int i = b[j];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return buf.toString();
	}

	/**
	 * @创建时间: 2020年12月1日 下午3:07:56
	 * @更新时间: 2020年12月1日 下午3:07:56
	 * @作者: LIUJIANFENG
	 * @param date
	 * @param format
	 * @return
	 * @描述: date要转换的日期
	 */
	public static String format(Date date, String format) {
		String dateVal = "";
		try {
			dateVal = new SimpleDateFormat(format).format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dateVal;
	}
}
